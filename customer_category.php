<?php 
    session_start();

    include 'init.php';
  $pagetitle="Categorise Of Customers";
 
          $do=isset($_GET['do'])? $_GET['do']:'Manage'; //check if do==what ?  ****************************
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

if($do=='Manage'){
    
    
    
    
    $sort='ASC';
    $sort_array=array('ASC','DESC');
    if(isset($_GET['sort']) && in_array($_GET['sort'], $sort_array) ){

      $sort=$_GET['sort'];

    }


     $stmt=$con->prepare("SELECT * FROM  customers_category  ORDER BY id $sort");
     $stmt->execute();
     $cat=$stmt->fetchAll(); ?>



<h1 class="text-center"> ادارة  فئات العملاء </h1>
<div class="container   Categories">
  <div class="panel  panel-default">
	<div class="panel-heading">

  <i class="fa fa-edit"></i>  ادارة فئات العملاء 
  <div class="option  pull-right">
  <i class="fa fa-sort"> </i> Ordering:[
      <a class="<?php if($sort == 'ASC'){ echo 'active';}?>" href="?sort=ASC">ASC</a> |
    <a class="<?php if($sort == 'DESC'){ echo 'active';}?>" href="?sort=DESC">DESC</a>]

     <i class="fa fa-eye"> </i>  View:[
    <span class="active"  data-view="full">Full</span>|
    <span data-view="classic">Classic</span>]
</div>
  </div>
    <div class="panel-body">
    	 <?php
           foreach ($cat as$ca) {
// echo $_SESSION['username'];
           echo '<div class="cat">';
           echo "<div class='hidden-button'>";
              echo "<a href='product_category.php?do=Edit&catid=".$ca['id'] ."' class='btn btn-xs btn-primary'>
              <i class='fa fa-edit'></i>Edit  </a>";
              echo "<a href='product_category.php?do=Delete&catid=".$ca['id'] ."'  class='  comfirm  btn btn-xs btn-danger'>
              <i class='fa fa-close'></i>Delete  </a>";


           echo "</div>";


           echo '<h3>'.$ca['name'].'</h3>';


       echo '</div>';
       echo '<hr>';
           }


    	 ?>


    </div>
</div>

<a href="customer_category.php?do=Add" class=" addbtn btn btn-primary"><i class="fa fa-plus"> </i> اضافة فئة </a>
</div>

<?php
}


elseif($do=='Add'){


?>


            <h1 class="text-center">اضافة فئة  </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=Insert" method="POST">

                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">الاسم </label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="namec" 
                      class="form-control" 
                      autocomplete="off" 
                      placeholder="Name Of Categorie" 
                      required="required"  />
                     </div>
                   </div>

   

                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="اضافة   " class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>

<?php
}

elseif($do == 'Insert'){
      if($_SERVER['REQUEST_METHOD']=='POST'){

           echo '<div class="container">';
           echo '<h1 class="text-center"> Insert Page</h1>';

           $cname		=$_POST['namec'];
        

$stmt=$con->prepare("INSERT INTO customers_category(name)
                           VALUES( :cname)
	                            ");
$stmt->execute(array(
                    'cname'=>$cname
	));
    $mas='<div  class="alert alert-success"> '.$stmt->rowCount().' Inserted'.'</div>';
    Redurict($mas,'back');          


      }else{

        echo '<div class="container">';
        $mas='<div  class="alert alert-danger">You Not Allow To Come Here </div> ';
        Redurict($mas,'jjj');


      }


	echo '</div>';

}


///////////////////////////////////////////////////////////////////////////////////////////////



elseif($do=='Edit'){ 
 $catid=isset($_GET['catid']) && is_numeric($_GET['catid'])? intval($_GET['catid']):0;
           $stmt=$con->prepare("SELECT * 
                                FROM 
                                customers_category
                                WHERE 
                                     id=? 
                                   ");
           $stmt->execute(array($catid));
           $row=$stmt->fetch();

            $count=$stmt->rowCount();

            if($stmt->rowCount() > 0){  ?> 

<h1 class="text-center">تعديل فئة العميل  </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=Update" method="POST">

                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">الاسم </label>
                     <div class="col-sm-10  col-md-6">
                      <input type="hidden" name="catid" value="<?php echo $row['id']; ?>" />

                      <input 
                      type="text" 
                      name="namec" 
                      class="form-control" 
                      placeholder="Name Of Categorie" 
                      required="required" 
                      value='<?php echo $row['name']; ?>'
                       />
                     </div>
                   </div>






                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="حفظ" class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>            
 <?php 
   /* if of end of edit page */
             }else{
              echo '<div class="container">';
              $mes='<div class="alert alert-danger">You are not alwoed to come here </div>';
              Redurict($mes);
              echo '</div>';

            }

}
//////////////////////////////////////////////////////////////////////////////////////////
         
elseif($do=='Update'){

  echo "<div class='container'>";
  echo '<h1 class="text-center">Update Category</h1>';


   if($_SERVER['REQUEST_METHOD']=='POST'){
      $catid         =$_POST['catid'];
      $catname       =$_POST['namec'];
     
      $s=$con->prepare("UPDATE 
                             customers_category 
                           SET
                             name=? 
                           WHERE 
                              id=? 
                                     ");

      $s->execute(array( $catname ,$catid ));

    $mas='<div class="alert alert-success">'.$s->rowCount() .' Record Updated </div>';
           Redurict($mas,'back');


    


   }else{


              echo '<div class="container">';
              $mas='<div class=" alert alert-danger"> YOU Not alowed to came here </div>';
              Redurict($mas,'back');
              echo '</div>';

   }


echo "</div>";
}

///////////////////////////////////////////////////////////////////////////////////////////////



elseif($do=='Delete'){ 

echo '<div class="container">';
echo '<h1 class="text-center"> Delete page </h1>';

$catid=isset($_GET['catid']) && is_numeric($_GET['catid'])? intval($_GET['catid']):0;

    $checkItem=checkItem('id','customers_category',$catid);

          if($checkItem > 0){  
                  $stmt=$con->prepare("DELETE FROM customers_category WHERE id=?");
                  $stmt->execute(array($catid));
                  if($stmt->rowCount() > 0){
ob_start();

                   $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          }

echo '</div>';

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
        include 'footer.php';

       ob_end_flush();

 //end of if check if is set session username camed or not****************************///////////////////////
   